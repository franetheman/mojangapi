package me.franetheman.mojangapi.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import me.franetheman.mojangapi.events.ServiceCheckEvent;

public class ServiceStatusChecker {

    private final JSONParser jsonParser = new JSONParser();
    
    public ServiceStatusChecker(){
	
    }
    
    public Map<Service,Status> getAllStates(){
	Map<Service, Status> stateList = new HashMap<>();
	try {
	    URL url = new URL(Service.ALL.getAddress());
	    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
	    
	    String s = reader.readLine();
	    reader.close();
	    
	    s = StringUtils.remove(s, '[');
	    s = StringUtils.remove(s, ']');
	    
	    String[] jsonObjects = StringUtils.split(s, ',');
	    
	    for(String json : jsonObjects){
		String service = StringUtils.substringBetween(json, "{\"", "\"");
		String status = StringUtils.substringBetween(json, ":\"", "\"}");
		
		stateList.put(getEnumService(service), getEnumStatus(status));
	    }
	    
	    Bukkit.getPluginManager().callEvent(new ServiceCheckEvent(stateList));
	    return stateList;
	} catch(Exception e){
	    e.printStackTrace();
	    return null;
	}
    }
    
    public Status getStatus(Service service){
	if(service == Service.ALL){
	    System.out.println("You cannot get all services status using getStatus(paramService)");
	    throw new IllegalArgumentException();
	}
	try {
	    URL url = new URL(service.getAddress());
	    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
	    
	    Object object = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) object;
            
            String status = jsonObject.toJSONString();
            reader.close();
            
            Bukkit.getPluginManager().callEvent(new ServiceCheckEvent(service, getEnumStatus(status)));
	    return getEnumStatus(status);
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }
    
    private Status getEnumStatus(String status){
	status = status.toLowerCase();
	switch (status) {
        case "green":
            return Status.ONLINE;
        case "yellow":
            return Status.UNSTABLE;
        case "red":
            return Status.OFFLINE;
        default:
            return Status.UNKNOWN;
        }
    }
    
    private Service getEnumService(String service){
	service = service.toLowerCase();
	switch(service){
	case "minecraft.net":
	    return Service.MINECRAFT_NET;
	case "session.minecraft.net":
	    return Service.SESSION_MINECRAFT_NET;
	case "account.mojang.com":
	    return Service.ACCOUNT_MOJANG_NET;
	case "auth.mojang.com":
	    return Service.AUTH_MOJANG_COM;
	case "skins.minecraft.net":
	    return Service.SKINS_MINECRAFT_NET;
	case "authserver.mojang.com":
	    return Service.AUTH_MOJANG_COM;
	case "sessionserver.mojang.com":
	    return Service.SESSIONSERVER_MOJANG_COM;
	case "api.mojang.com":
	    return Service.API_MOJANG_COM;
	case "textures.minecraft.net":
	    return Service.TEXTURES_MINECRAFT_NET;
	    
	    default:
		throw new RuntimeException();
	}
    }
}
