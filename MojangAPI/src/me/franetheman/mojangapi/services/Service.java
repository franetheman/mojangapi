package me.franetheman.mojangapi.services;

public enum Service {
    ALL("http://status.mojang.com/check", "all"),
    MINECRAFT_NET("http://status.mojang.com/check?service=minecraft.net", "minecraft.net"),
    SESSION_MINECRAFT_NET("http://status.mojang.com/check?service=session.minecraft.net", "session.minecraft.net"),
    ACCOUNT_MOJANG_NET("http://status.mojang.com/check?service=account.mojang.com", "account.mojang.com"),
    AUTH_MOJANG_COM("http://status.mojang.com/check?service=auth.mojang.com", "auth.mojang.com"),
    SKINS_MINECRAFT_NET("http://status.mojang.com/check?service=skins.minecraft.net", "skins.minecraft.net"),
    AUTHSERVER_MOJANG_COM("http://status.mojang.com/check?service=authserver.mojang.com", "authserver.mojang.com"),
    SESSIONSERVER_MOJANG_COM("http://status.mojang.com/check?service=sessionserver.mojang.com", "sessionserver.mojang.com"),
    API_MOJANG_COM("http://status.mojang.com/check?service=api.mojang.com", "api.mojang.com"),
    TEXTURES_MINECRAFT_NET("http://status.mojang.com/check?service=textures.minecraft.net", "textures.minecraft.net");
    
    private String address, serviceName;
    
    private Service(String address, String serviceName){
	this.address = address;
	this.serviceName = serviceName;
    }
    
    public String getAddress(){
	return address;
    }
    
    public String getServiceName(){
	return serviceName;
    }
}
