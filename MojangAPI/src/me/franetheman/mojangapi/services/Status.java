package me.franetheman.mojangapi.services;

public enum Status {

    ONLINE("Online", "This service is healthy"),
    UNSTABLE("Unstable", "This service is currently experiencing issues"),
    OFFLINE("Offline", "This service is offline"),
    UNKNOWN("Unknown", "Couldn't connect to Mojang!");

    private String status, description;
    
    private Status(String status, String description){
	this.status = status;
	this.description = description;
    }
    
    public String getStatus(){
	return status;
    }
    
    public String getDescription(){
	return description;
    }
}
