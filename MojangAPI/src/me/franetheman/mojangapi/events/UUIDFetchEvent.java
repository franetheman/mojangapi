package me.franetheman.mojangapi.events;

import java.util.UUID;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class UUIDFetchEvent extends Event{

    private static final HandlerList HANDLERS = new HandlerList();

    private String name;
    private UUID returnedUUID;
    private String returnedRawUUID;
    private int timestamp;
    
    public UUIDFetchEvent(String name,UUID returnedUUID){
	this.name = name;
	this.returnedUUID = returnedUUID;
    }
    
    public UUIDFetchEvent(String name,UUID returnedUUID, int timeStamp){
	this.name = name;
	this.returnedUUID = returnedUUID;
	this.timestamp = timeStamp;
    }
    
    public UUIDFetchEvent(String name,String returnedRawUUID){
	this.name = name;
	this.returnedRawUUID = returnedRawUUID;
    }
    
    public String getName() {
        return name;
    }

    public UUID getReturnedUUID() {
        return returnedUUID;
    }

    public String getReturnedRawUUID() {
        return returnedRawUUID;
    }

    public int getTimestamp() {
        return timestamp;
    }

    @Override
    public HandlerList getHandlers() {
	return HANDLERS;
    }
    
    public static HandlerList getHandlerList(){
	return HANDLERS;
    }
}
