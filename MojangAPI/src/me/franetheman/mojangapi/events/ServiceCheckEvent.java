package me.franetheman.mojangapi.events;

import java.util.Map;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.franetheman.mojangapi.services.Service;
import me.franetheman.mojangapi.services.Status;

public class ServiceCheckEvent extends Event{

    private static final HandlerList HANDLERS = new HandlerList();

    private Service service;
    private Status status;
    private Map<Service,Status> serviceStates;
    
    public ServiceCheckEvent(Service service,Status status){
	this.service = service;
	this.status = status;
    }
    
    public ServiceCheckEvent(Map<Service,Status> serviceStates){
	this.serviceStates = serviceStates;
    }
    
    public Service getService(){
	return service;
    }
    
    public Status getStatus(){
	return status;
    }
    
    public Map<Service,Status> getServiceStates(){
	return serviceStates;
    }
    
    @Override
    public HandlerList getHandlers() {
	return HANDLERS;
    }
    
    public static HandlerList getHandlerList(){
	return HANDLERS;
    }
    
}
