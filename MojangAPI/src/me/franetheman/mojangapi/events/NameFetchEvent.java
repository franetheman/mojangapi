package me.franetheman.mojangapi.events;

import java.util.List;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NameFetchEvent extends Event {

    private static final HandlerList HANDLERS = new HandlerList();

    private String name;
    private List<String> nameHistory;
    private String rawUUID;

    public NameFetchEvent(String name,
	    String rawUUID) {
	this.name = name;
	this.rawUUID = rawUUID;
    }
    
    public NameFetchEvent(String name, List<String> nameHistory,
	    String rawUUID) {
	this.name = name;
	this.nameHistory = nameHistory;
	this.rawUUID = rawUUID;
    }

    public String getName() {
	return name;
    }

    public List<String> getNameHistory() {
	return nameHistory;
    }

    public String getRawUUID() {
	return rawUUID;
    }

    @Override
    public HandlerList getHandlers() {
	return HANDLERS;
    }

    public static HandlerList getHandlerList() {
	return HANDLERS;
    }
}
