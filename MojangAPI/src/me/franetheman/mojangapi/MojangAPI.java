package me.franetheman.mojangapi;

import org.bukkit.plugin.java.JavaPlugin;

import me.franetheman.mojangapi.player.NameFetcher;
import me.franetheman.mojangapi.player.UUIDFetcher;
import me.franetheman.mojangapi.services.ServiceStatusChecker;

/*
 * Copyright 2015 Frans Sontag (franetheman). All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and contributors and should not be interpreted as representing official policies,
 * either expressed or implied, of anybody else.
 */
public class MojangAPI extends JavaPlugin{
    
    private static MojangAPI instance;
    
    private NameFetcher nameFetcher;
    private UUIDFetcher uuidFetcher;
    private ServiceStatusChecker serviceChecker;
    
    public void onEnable(){
	instance = this;
	
	nameFetcher = new NameFetcher();
	uuidFetcher = new UUIDFetcher();
	serviceChecker = new ServiceStatusChecker();
    }
    
    public static MojangAPI getInstance(){
	return instance;
    }
    
    public NameFetcher getNameFetcher(){
	return nameFetcher;
    }
    
    public UUIDFetcher getUUIDFetcher(){
	return uuidFetcher;
    }
    
    public ServiceStatusChecker getServiceChecker(){
	return serviceChecker;
    }
}
