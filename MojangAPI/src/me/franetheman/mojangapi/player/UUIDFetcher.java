package me.franetheman.mojangapi.player;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import me.franetheman.mojangapi.events.UUIDFetchEvent;

public class UUIDFetcher {

    private static final String UUID_ADDRESS = "https://api.mojang.com/users/profiles/minecraft/";
    private final JSONParser jsonParser = new JSONParser();

    public UUID getPlayerUUID(String playerName) {
	try {
	    URL url = new URL(UUID_ADDRESS + playerName);
	    BufferedReader reader = new BufferedReader(
		    new InputStreamReader(url.openStream()));

	    Object object = jsonParser.parse(reader);
	    JSONObject jsonObject = (JSONObject) object;

	    reader.close();

	    String uid = (String) jsonObject.get("id");
	    
	    Bukkit.getPluginManager().callEvent(new UUIDFetchEvent(playerName, getUUIDFromNonDashedString(uid)));
	    return getUUIDFromNonDashedString(uid);
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }
    
    public UUID getPlayerUUIDAtTime(String playerName,int timestamp) {
	try {
	    URL url = new URL(UUID_ADDRESS + playerName + "?at=" + timestamp);
	    BufferedReader reader = new BufferedReader(
		    new InputStreamReader(url.openStream()));

	    Object object = jsonParser.parse(reader);
	    JSONObject jsonObject = (JSONObject) object;

	    reader.close();

	    String uid = (String) jsonObject.get("id");
	    
	    Bukkit.getPluginManager().callEvent(new UUIDFetchEvent(playerName, getUUIDFromNonDashedString(uid), timestamp));
	    return getUUIDFromNonDashedString(uid);
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }

    public String getRawPlayerUUID(String playerName) {
	try {
	    URL url = new URL(UUID_ADDRESS + playerName);
	    BufferedReader reader = new BufferedReader(
		    new InputStreamReader(url.openStream()));

	    Object object = jsonParser.parse(reader);
	    JSONObject jsonObject = (JSONObject) object;

	    reader.close();

	    String uid = (String) jsonObject.get("id");
	    Bukkit.getPluginManager().callEvent(new UUIDFetchEvent(playerName, uid));
	    return uid;
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }
    
    private UUID getUUIDFromNonDashedString(String id) {
	return UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12)
		+ "-" + id.substring(12, 16) + "-" + id.substring(16, 20) + "-"
		+ id.substring(20, 32));
    }
}
