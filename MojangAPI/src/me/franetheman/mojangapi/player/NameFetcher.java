package me.franetheman.mojangapi.player;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import me.franetheman.mojangapi.events.NameFetchEvent;

@SuppressWarnings("rawtypes")
public class NameFetcher {

    private static final String NAME_HISTORY = " https://api.mojang.com/user/profiles/";
    private final JSONParser jsonParser = new JSONParser();

    public String getCurrentName(String rawUUID) {
	try {
	    URL url = new URL(NAME_HISTORY + rawUUID + "/names");
	    BufferedReader reader = new BufferedReader(
		    new InputStreamReader(url.openStream()));
	    
	    ArrayList list = (ArrayList) jsonParser.parse(reader);
	    reader.close();
	    JSONObject jsObject = (JSONObject) list.get(list.size()-1);
	    
	    Bukkit.getPluginManager().callEvent(new NameFetchEvent((String)jsObject.get("name"), rawUUID));
	    return (String)jsObject.get("name");
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }

    public List<String> getNameHistory(String rawUUID) {
	try {
	    URL url = new URL(NAME_HISTORY + rawUUID + "/names");
	    BufferedReader reader = new BufferedReader(
		    new InputStreamReader(url.openStream()));
	    List<String> names = new ArrayList<>();
	    
	    ArrayList objects = (ArrayList) jsonParser.parse(reader);
	    reader.close();
	    
	    for(Object object : objects){
		names.add((String)((JSONObject)object).get("name"));
	    }
	    
	    Bukkit.getPluginManager().callEvent(new NameFetchEvent((String)((JSONObject)objects.get(objects.size()-1)).get("name"), names, rawUUID));
	    return names;
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }
}
