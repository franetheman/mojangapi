# MojangAPI #


### Project Details ###

* Name: MojangAPI
* Version: 1.0

### How to set up? ###

* Download the latest MojangAPI jar from [the download page](https://bitbucket.org/franetheman/mojangapi/downloads)
* Place the jar inside your plugin folder
* When using it remember to add depend: [MojangAPI] to your plugin.yml

### Documentation ###

* Coming soon